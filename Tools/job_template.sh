#!/bin/sh
#SBATCH --partition=__PARTITION__
#SBATCH --time=__TIME__
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=12
#SBATCH --constraint=CPU-E5645
#SBATCH --job-name="OTU_picking"
#SBATCH --output=OTU_picking.log

set -e
# set +x
TOTAL_START=`date +%s`

echo '--------------------'
echo 'loading modules ...'
module load python/anaconda
module load R/3.1.2
module load qiime/1.9.0
source activate /util/academic/qiime/1.9.0.dev
PATH=/projects/academic/yijunsun/lu/app:$PATH

echo '--------------------'
echo 'preprocessing...'
START=`date +%s`
python Tools/select_fastq.py
cp Selected_fastq/*.fastq.gz Selected_Count/
cd Selected_Count
gunzip *
count_seqs.py -i "*.fastq" -o ../Result/no.selected.counts.txt
cd ..
multiple_join_paired_ends.py -i Selected_fastq/ -o Joined/ -p Doc/parameter.txt
python Doc/JoinCount_prepare.py
cd JoinCount
count_seqs.py -i "*.fastq" -o ../Result/no.join.counts.txt
cd ..
python Tools/qf_prepare.py
multiple_split_libraries_fastq.py -i Quality_filtered/ -o Quality_filtered/ --sampleid_indicator . -p Doc/parameter.txt
cd Quality_filtered
count_seqs.py -i seqs.fna -o ../Result/no.totalsequences.passed.qf.txt
cat seqs.fna | grep ">" > quality.filtered.sequences.per.sample.txt
cut -f1 -d"_" quality.filtered.sequences.per.sample.txt > quality.filtered.sequences.per.sample.f1.txt
cut -f2 -d">" quality.filtered.sequences.per.sample.f1.txt > quality.filtered.sequences.per.sample.f2.txt
sort quality.filtered.sequences.per.sample.f2.txt > quality.filtered.sequences.per.sample.sorted.txt
uniq -c quality.filtered.sequences.per.sample.sorted.txt > ../Resul/no.quality.filtered.sequences.per.sample.txt
cd ..
END=`date +%s`
ELAPSED=$(( $END - $START ))
echo "preprocessing takes $ELAPSED s"

echo '--------------------'
echo 'CLOSED picking...'
START=`date +%s`
pick_closed_reference_otus.py -i Quality_filtered/seqs.fna  -o Closed_Ref/  -r Database/gg/97_otus.fasta -t Database/gg/97_otu_taxonomy.txt -a -O 12 -s
END=`date +%s`
ELAPSED=$(( $END - $START ))
echo "CLOSED picking takes $ELAPSED s"

echo '--------------------'
echo 'table filtering...'
START=`date +%s`
cp Closed_Ref/otu_table.biom CLOSED_tables/otu_table_w_tax.biom
cp Closed_Ref/rep_set/seqs_rep_set.fasta CLOSED_tables
cd CLOSED_tables
parallel_align_seqs_pynast.py -i seqs_rep_set.fasta -o pynast_aligned_seqs -O 12
filter_samples_from_otu_table.py -i otu_table_w_tax.biom -o otu_table_sample_filtered.biom -n __SAMPLE_ABUNDANCE__
filter_otus_from_otu_table.py -i otu_table_sample_filtered.biom -o otu_table_sample_otu_filtered.biom --min_count_fraction=__OTU_ABUNDANCE__ -e pynast_aligned_seqs/otus_failures.fasta
cp otu_table_sample_otu_filtered.biom final_table.biom
biom convert -i final_table.biom -o final_table.txt --to-tsv --header-key taxonomy
summarize_taxa.py -i final_table.biom -L 2,3,4,5,6,7 -o sum_taxa -a
cd ..
END=`date +%s`
ELAPSED=$(( $END - $START ))
echo "table filtering takes $ELAPSED s"

echo '--------------------'
echo 'negative sample checking'
START=`date +%s`
module load ncbi/blast-2.2.29
python Tools/negative_prepare.py
cp Database/gg/* Negative/blast/
cd Negative/blast/
makeblastdb -in 97_otus.fasta -out gg -dbtype 'nucl' -input_type fasta -logfile gg.log
for f in *Negative*; do 
    blastn -query $f -task blastn -db gg -num_threads 12 -perc_identity 90 -evalue 1e-20 -max_target_seqs 10 -outfmt "7 qacc sacc pident qcovs" -out $f".blast.txt"
    python ../../Tools/label.py $f".blast.txt" 97_otu_taxonomy.txt $f".labeled.txt"
done
cp *labeled.txt ../result
cd ../..
END=`date +%s`
ELAPSED=$(( $END - $START ))
echo "negative checking takes $ELAPSED s"


TOTAL_END=`date +%s`
TOTAL_ELAPSED=$(( $TOTAL_END - $TOTAL_START ))
echo "total takes $TOTAL_ELAPSED s"
